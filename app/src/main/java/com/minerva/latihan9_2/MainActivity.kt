package com.minerva.latihan9_2

import android.Manifest
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.v4.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.Toast
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.util.Log
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import com.minerva.latihan9_2.R.id.seekBar



val musicDirectory = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).toString())
val myHandler = Handler()
val _songs = ArrayList<SongInfo>()

class MainActivity : AppCompatActivity() {
    private var mediaPlayer: MediaPlayer? = null
    //    private val _songs = ArrayList<SongInfo>()
//    private val myHandler = Handler()
//    private var index = 0
    var isplay = false
    val permission: kotlin.Array<String> = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
    var files = musicDirectory.listFiles()
    val musicPref = musicSharedPref(this@MainActivity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaPlayer = MediaPlayer()
        checkUserPermission()
        val t = runThread()
        t.start()

        playBtn.setOnClickListener {
            if(!musicPref.mediaPlayer!!.isPlaying){
                musicPref.playS()
            }
            else{
                musicPref.pauseSong()
            }

        }
        skipNextBtn.setOnClickListener {
            musicPref.nextSong()
        }
        skipPreviousBtn.setOnClickListener {
            musicPref.prevSong()
        }

    }
    override fun onResume() {
        super.onResume()
    }
    override fun onStop() {
        super.onStop()
        val appWidgetManager = AppWidgetManager.getInstance(this)
        val ids = appWidgetManager.getAppWidgetIds(
            ComponentName(this, MyMusicWidget::class.java)
        )
        val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        sendBroadcast(updateIntent)
    }
//    fun fastforward(){
//        if(mediaPlayer!!.currentPosition + 5000 < mediaPlayer!!.duration!!.toInt()){
//            mediaPlayer!!.seekTo(mediaPlayer!!.currentPosition + 5000)
//        }
//    }
//
//    fun fastrewind(){
//        if(mediaPlayer!!.currentPosition - 5000 > 0){
//            mediaPlayer!!.seekTo(mediaPlayer!!.currentPosition - 5000)
//        }
//    }

//    fun prevSong(){
//
//        mediaPlayer!!.stop()
//        mediaPlayer!!.reset()
//        mediaPlayer!!.release()
//        mediaPlayer = null
//        index -= 1
//        if(index < 0){
//            index = files.count()-1
//        }
//        playSong(files[index].toString())
//
//    }
//    fun nextSong(){
//
//        mediaPlayer!!.stop()
//        mediaPlayer!!.reset()
//        mediaPlayer!!.release()
//        mediaPlayer = null
//        index += 1
//        if(index > files.count()-1){
//            index = 0
//        }
//        playSong(files[index].toString())
//    }
//
//    fun pauseSong(){
//
//        mediaPlayer!!.pause()
//
//    }
//
//    fun stopSong(){
//        mediaPlayer!!.stop()
//        mediaPlayer!!.reset()
//        mediaPlayer!!.release()
//        mediaPlayer = null
//    }
//    fun playSong(music : String){
//        val runnable = object : Runnable, MediaPlayer.OnPreparedListener {
//            override fun onPrepared(mp: MediaPlayer?) {
//
//            }
//
//            override fun run() {
//                try {
//                    mediaPlayer = MediaPlayer()
//                    mediaPlayer!!.setDataSource(music)
//                    mediaPlayer!!.prepareAsync()
//                    mediaPlayer!!.setOnPreparedListener(MediaPlayer.OnPreparedListener() { mp ->
//                        mp.start()
//                        seekBar.progress = 0
//                        seekBar.max = mediaPlayer!!.duration
//
//                        var date = Date(mediaPlayer!!.duration.toLong())
//                        var format = SimpleDateFormat("mm:ss")
//                        namaLagu.text = "Nama Lagu :  ${files[index].nameWithoutExtension}"
//                        duration.text = "Durasi : ${format.format(date)}"
//                    })
//                }
//                catch (e: Exception) {
//                }
//            }
//
//        }
//        myHandler.postDelayed(runnable, 100)
//
//    }

    fun checkUserPermission(){
        if(Build.VERSION.SDK_INT >= 23){
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),123)
                return
            }
            else{
                musicPref.playSong(files[musicPref.index].toString())
            }
        }
        loadSongs()
    }

    inner class runThread : Thread() {
        override fun run() {
            while (true) {
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                if (mediaPlayer != null) {
                    seekBar.post { seekBar.progress = mediaPlayer!!.getCurrentPosition() }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            123 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadSongs()
            } else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                checkUserPermission()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun loadSongs() {

        val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val selection = MediaStore.Audio.Media.IS_MUSIC + "!=0"
        val cursor = contentResolver.query(uri, null, selection, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME))
                    val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                    val url = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))
                    val duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION))
                    val s = SongInfo(name, artist, url,duration)
                    _songs.add(s)

                } while (cursor.moveToNext())
            }
            cursor.close()
        }
    }


}
