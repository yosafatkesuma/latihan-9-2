package com.minerva.latihan9_2

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.widget.RemoteViews
import java.util.ArrayList

/**
 * Implementation of App Widget functionality.
 */
class MyMusicWidget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }

    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        val musicPref = musicSharedPref(context)
        if(PlayOnClick.equals(intent.action)){
            if(!musicPref.mediaPlayer!!.isPlaying){
                musicPref.playS()
            }
            else{
                musicPref.pauseSong()
            }

        }
//        else if(PauseOnClick.equals(intent.getAction())){
//            musicPref.pauseSong()
//
//        }
        else if(NextOnClick.equals(intent.action)){
            musicPref.nextSong()
        }
        else if(PrevOnClick.equals(intent.action)){

            musicPref.prevSong()
        }
    }

    companion object {
        private val PlayOnClick = "PlayOnClick"
        private val NextOnClick = "NextOnClick"
        private val PrevOnClick = "PrevOnClick"
        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {

//            val widgetText = context.getString(R.string.appwidget_text)
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.my_music_widget)
            views.setOnClickPendingIntent(R.id.playBtn, getPendingSelfIntent(context, PlayOnClick))
            views.setOnClickPendingIntent(R.id.skipPreviousBtn, getPendingSelfIntent(context, PrevOnClick))
            views.setOnClickPendingIntent(R.id.skipNextBtn, getPendingSelfIntent(context, NextOnClick))

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
        protected fun getPendingSelfIntent(context: Context,
                                           buttonStr: String): PendingIntent {
            val intent = Intent(context, MyMusicWidget::class.java)
            intent.action = buttonStr
            return PendingIntent.getBroadcast(context, 0, intent, 0)
        }
    }
}

