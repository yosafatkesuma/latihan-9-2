package com.minerva.latihan9_2

class SongInfo {
    private var Songname: String? = null
    private var Artistname: String? = null
    private var SongUrl: String? = null
    var duration: String? = null

    constructor(songname: String, artistname: String, songUrl: String, duration: String) {
        this.Songname = songname
        this.Artistname = artistname
        this.SongUrl = songUrl
        this.duration = duration

    }
}