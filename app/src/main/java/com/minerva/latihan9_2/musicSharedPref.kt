package com.minerva.latihan9_2

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.requestPermissions
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

//var index = 0
class musicSharedPref(context: Context){


    var mediaPlayer: MediaPlayer? = null
    //    private val _songs = ArrayList<SongInfo>()
    var index = 0
    var isplay = false
    var position = 0
    var files = musicDirectory.listFiles()
    fun playS(){
        if(!mediaPlayer!!.isPlaying){
            if(position!=0){
                mediaPlayer!!.seekTo(position)
                mediaPlayer!!.start()
                position=0
            }
        }
    }
    fun prevSong(){
        mediaPlayer!!.stop()
        mediaPlayer!!.reset()
        mediaPlayer!!.release()
        mediaPlayer = null
        index -= 1
        if(index < 0){
            index = files.count()-1
        }
        playSong(files[index].toString())
    }

    fun nextSong(){
        mediaPlayer!!.stop()
        mediaPlayer!!.reset()
        mediaPlayer!!.release()
        mediaPlayer = null
        index += 1
        if(index > files.count()-1){
            index = 0
        }
        playSong(files[index].toString())
    }

    fun pauseSong(){
        mediaPlayer!!.pause()
        position = mediaPlayer!!.currentPosition

    }

    fun playSong(music : String){
        val runnable = object : Runnable, MediaPlayer.OnPreparedListener {
            override fun onPrepared(mp: MediaPlayer?) {

            }

            override fun run() {
                try {
                    mediaPlayer = MediaPlayer()
                    mediaPlayer!!.setDataSource(music)
                    mediaPlayer!!.prepareAsync()
                    mediaPlayer!!.setOnPreparedListener(MediaPlayer.OnPreparedListener() { mp ->
                        mp.start()
//                        seekBar.progress = 0
//                        seekBar.max = mediaPlayer!!.duration
//
//                        var date = Date(mediaPlayer!!.duration.toLong())
//                        var format = SimpleDateFormat("mm:ss")
//                        namaLagu.text = "Nama Lagu :  ${files[index].nameWithoutExtension}"
//                        duration.text = "Durasi : ${format.format(date)}"
                    })
                }
                catch (e: Exception) {
                }
            }

        }
        myHandler.postDelayed(runnable, 100)

    }

}